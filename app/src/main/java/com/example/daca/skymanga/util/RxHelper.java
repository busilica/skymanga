package com.example.daca.skymanga.util;

import com.example.daca.skymanga.chapterlist.Chapter;
import com.example.daca.skymanga.main.Manga;

import java.util.ArrayList;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Daca on 25.04.2017..
 */

public class RxHelper {

    public Observable<ArrayList<Manga>> buildMangaObservable(String response){
        Observable<ArrayList<Manga>> observable = Observable.just(response)
                .map(new Function<String, ArrayList<Manga>>() {
                    @Override
                    public ArrayList<Manga> apply(String s) throws Exception {
                        return new JsonParser().parseManga(s);
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
        return observable;
    }

    public Observable<ArrayList<Chapter>> buildChaptersListObservable(String response){
        Observable<ArrayList<Chapter>> observable = Observable.just(response)
                .map(new Function<String, ArrayList<Chapter>>() {
                    @Override
                    public ArrayList<Chapter> apply(String s) throws Exception {
                        return new JsonParser().parseChaptersList(s);
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        return observable;
    }

    public Observable<ArrayList<String>> buildChapterObservable(String response) {
        Observable<ArrayList<String>> observable = Observable.just(response)
                .map(new Function<String, ArrayList<String>>() {
                    @Override
                    public ArrayList<String> apply(String s) throws Exception {
                        return new JsonParser().parseChapter(s);
                    }
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        return observable;
    }
}
