package com.example.daca.skymanga.chapter;

import java.util.ArrayList;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by Daca on 14.04.2017..
 */

public class ObserverBuilder {

    private ChapterActivity activity;

    public ObserverBuilder(ChapterActivity activity) {
        this.activity = activity;
    }

    public Observer<ArrayList<String>> getChapterObserver(){
        Observer<ArrayList<String>> observer = new Observer<ArrayList<String>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(ArrayList<String> value) {
                activity.setupRvAdapter(value);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
        return observer;
    }
}
