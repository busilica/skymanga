package com.example.daca.skymanga.util;

import com.example.daca.skymanga.chapterlist.Chapter;
import com.example.daca.skymanga.main.Manga;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Daca on 25.04.2017..
 */

public class JsonParser {

    public ArrayList<Manga> parseManga(String data) {
        ArrayList<Manga> list = new ArrayList<>();
        try {
            JSONObject object1 = new JSONObject(data);
            JSONArray array = object1.getJSONArray("manga");
            for (int i = 0; i < array.length(); i++) {
                JSONObject object2 = array.getJSONObject(i);
                String title = object2.getString("t");
                String image = object2.getString("im");
                String id    = object2.getString("i");
                String hits  = object2.getString("h");
                JSONArray c  = object2.getJSONArray("c");
                boolean skip = false;
                for (int j = 0; j < c.length(); j++) {
                    String category = c.getString(j);
                    if (category.contains("Yaoi") ||
                        category.contains("Yuri") ||
                        category.contains("Doujinshi") ||
                        category.contains("Shoujo") ||
                        category.contains("Adult") ||
                        category.contains("Smut")){
                            skip = true;
                    }
                }
                Manga manga = new Manga(title, image, id, hits, c);
                if (Integer.valueOf(manga.getHits()) > 10000 && !skip){
                    list.add(manga);
                }
                //list.add(new Manga(title, image, id, hits));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<Chapter> parseChaptersList(String data) {
        ArrayList<Chapter> list = new ArrayList<>();
        try {
            JSONObject object1 = new JSONObject(data);
            JSONArray array = object1.getJSONArray("chapters");
            for (int i = 0; i < array.length(); i++) {
                JSONArray array2     = array.getJSONArray(i);
                String chapterNumber = array2.getString(0);
                String chapterTitle  = array2.getString(2);
                String id            = array2.getString(3);
                String title         = object1.getString("title");
                String description   = object1.getString("description");
                String imageUrl      = object1.getString("imageURL");
                Chapter chapter      = new Chapter(chapterTitle, chapterNumber, id, title,
                                                   description, imageUrl);
                list.add(chapter);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<String> parseChapter(String s) {
        ArrayList<String> list = new ArrayList<>();
        try {
            JSONObject object = new JSONObject(s);
            JSONArray array = object.getJSONArray("images");
            for (int i = 0; i < array.length(); i++) {
                JSONArray array2 = array.getJSONArray(i);
                String image     = array2.getString(1);
                list.add(image);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }
}
