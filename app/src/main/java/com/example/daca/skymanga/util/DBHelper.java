package com.example.daca.skymanga.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.daca.skymanga.main.Manga;


import java.util.ArrayList;


public class DBHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME    =   "notes.db";
    private static final int    DATABASE_VERSION =   1;
    private static final String TABLE_NAME       =   "notes";
    private static final String COLUMN_ID        =   "_id";
    private static final String COLUMN_1         =   "title";
    private static final String COLUMN_2         =   "image";
    private static final String COLUMN_3         =   "mangaId";
    private static final String COLUMN_4         =   "hits";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
       db.execSQL(
               "CREATE TABLE " + TABLE_NAME
               + " (" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                      + COLUMN_1 + " TEXT,"
                      + COLUMN_2 + " TEXT,"
                      + COLUMN_3 + " TEXT,"
                      + COLUMN_4 + " TEXT);"
       );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public boolean insertRow(Manga manga){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_1, manga.getTitle());
        contentValues.put(COLUMN_2, manga.getImage());
        contentValues.put(COLUMN_3, manga.getId());
        contentValues.put(COLUMN_4, manga.getHits());
        db.insert(TABLE_NAME, null, contentValues);
        return true;
    }


    public void deleteRow(String recipe_id){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = COLUMN_3 + " = ?";
        db.delete(TABLE_NAME, query, new String[]{recipe_id});
    }

    public boolean getRow(String searchKey){
        String query = "select * from " + TABLE_NAME + " where " + COLUMN_3 + " = ?";
        return getReadableDatabase().rawQuery(query, new String[]{searchKey}).moveToFirst();
    }

    public ArrayList<Manga> getSavedManga(){
        ArrayList<Manga> arrayList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor result = db.rawQuery("select * from " + TABLE_NAME, null);
        result.moveToFirst();
        while (!result.isAfterLast()){
            int id           = result.getInt(result.getColumnIndex(COLUMN_ID));
            String title     = result.getString(result.getColumnIndex(COLUMN_1));
            String image     = result.getString(result.getColumnIndex(COLUMN_2));
            String recipeId  = result.getString(result.getColumnIndex(COLUMN_3));
            String sourceUrl = result.getString(result.getColumnIndex(COLUMN_4));
            arrayList.add(new Manga(title, image, recipeId, sourceUrl, null));
            result.moveToNext();
        }
        result.close();
        return arrayList;
    }
}
