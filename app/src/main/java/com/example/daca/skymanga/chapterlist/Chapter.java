package com.example.daca.skymanga.chapterlist;

/**
 * Created by daca-pc on 23.04.2017..
 */

public class Chapter {

    private String chapterTitle;
    private String chapterNumber;
    private String id;
    private String title;
    private String description;
    private String imageUrl;

    public Chapter(){

    }

    public Chapter(String chapterTitle, String chapterNumber, String id, String title,
                   String description, String imageUrl) {
        this.chapterTitle = chapterTitle;
        this.chapterNumber = chapterNumber;
        this.id = id;
        this.title = title;
        this.description = description;
        this.imageUrl = imageUrl;
    }

    public String getChapterTitle() {
        return chapterTitle;
    }

    public void setChapterTitle(String chapterTitle) {
        this.chapterTitle = chapterTitle;
    }

    public String getChapterNumber() {
        return chapterNumber;
    }

    public void setChapterNumber(String chapterNumber) {
        this.chapterNumber = chapterNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
