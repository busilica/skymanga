package com.example.daca.skymanga.util;

import android.content.Context;
import android.util.Log;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import io.reactivex.Observer;


/**
 * Created by Daca on 25.04.2017..
 */

public class VolleyLoader implements Response.ErrorListener {

    private RequestQueue queue;
    private RxHelper rxHelper;

    public VolleyLoader(Context context) {
        queue = ConnectionManager.getInstance(context);
        rxHelper = new RxHelper();
    }

    public void load(final Observer observer, final String url, final String cmd) {
        final Response.Listener<String> listener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                switch (cmd){
                    case "loadManga":
                        rxHelper.buildMangaObservable(response).subscribe(observer);
                        break;
                    case "loadChaptersList":
                        rxHelper.buildChaptersListObservable(response).subscribe(observer);
                        break;
                    case "loadChapter":
                        rxHelper.buildChapterObservable(response).subscribe(observer);
                        break;
                }
            }
        };

        StringRequest request = new StringRequest(Request.Method.GET, url, listener, this);
        queue.add(request);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.e("Error", error.toString());
    }
}
