package com.example.daca.skymanga.chapterlist.recyclerview;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.example.daca.skymanga.R;
import com.example.daca.skymanga.chapter.ChapterActivity;
import com.example.daca.skymanga.chapterlist.Chapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by daca-pc on 23.04.2017..
 */

public class RvAdapter extends RecyclerView.Adapter<RvHolder> {

    private ArrayList<Chapter> list = new ArrayList<>();
    private Activity activity;
    private LayoutInflater inflater;

    public RvAdapter(ArrayList<Chapter> list, Activity activity) {
        this.list = list;
        this.activity = activity;
        inflater = activity.getLayoutInflater();
    }

    @Override
    public RvHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.activity_chapters_list_custom_row, parent, false);
        return new RvHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RvHolder holder, int position) {
        if (position == 0){
            Chapter chapter = list.get(position);
            Picasso.with(activity).load(chapter.getImageUrl()).into(holder.chapterImage);
            holder.title.setText(chapter.getTitle());
            holder.chapterNumber.setText(chapter.getDescription());
        }else {
            final Chapter chapter = list.get(position-1);
            holder.chapterImage.setVisibility(View.GONE);
            holder.title.setText(chapter.getChapterTitle());
            holder.chapterNumber.setText(chapter.getChapterNumber());
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String id = chapter.getId();
                    activity.startActivity(new Intent(activity, ChapterActivity.class)
                                                     .putExtra("id", id));
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
