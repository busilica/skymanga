package com.example.daca.skymanga.main;

import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.example.daca.skymanga.R;


/**
 * Created by Daca on 22.04.2017..
 */

/*
 *   Not activity,
 *   handles onCreateOptionsMenu and onOptionsItemSelected
 */
public abstract class OptionsActivity extends AppCompatActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_one_row:
                changeLayout(0);
                break;
            case R.id.menu_two_rows:
                changeLayout(1);
                break;
            case R.id.menu_three_rows:
                changeLayout(2);
                break;
            case R.id.theme_light:
                //setTheme(R.style.ActivityThemeLight);
                changeTheme(0);
                break;
            case R.id.theme_dark:
                //setTheme();
                changeTheme(1);
                break;
            case R.id.theme_sol_light:
                //setTheme();
                changeTheme(2);
                break;
            case R.id.theme_night_blue:
                //setTheme();
                changeTheme(3);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public abstract void changeLayout(int layout);

    public abstract void changeTheme(int theme);


}
