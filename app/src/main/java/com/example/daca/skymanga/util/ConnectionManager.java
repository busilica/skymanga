package com.example.daca.skymanga.util;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by Daca on 25.04.2017..
 */

public class ConnectionManager {

    private static RequestQueue queue;

        public static RequestQueue getInstance(Context context){
            if (queue == null){
                queue = Volley.newRequestQueue(context);
            }
        return queue;
    }
}
