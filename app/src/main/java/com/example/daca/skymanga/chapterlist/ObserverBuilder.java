package com.example.daca.skymanga.chapterlist;

import java.util.ArrayList;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by Daca on 14.04.2017..
 */

public class ObserverBuilder {

    private ChaptersListActivity activity;

    public ObserverBuilder(ChaptersListActivity activity) {
        this.activity = activity;
    }

    public Observer<ArrayList<Chapter>> getChaptersListObserver(){
        Observer<ArrayList<Chapter>> observer = new Observer<ArrayList<Chapter>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(ArrayList<Chapter> value) {
                activity.setupRvAdapter(value);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
        return observer;
    }
}
