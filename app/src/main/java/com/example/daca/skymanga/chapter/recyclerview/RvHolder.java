package com.example.daca.skymanga.chapter.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.example.daca.skymanga.R;


/**
 * Created by daca-pc on 24.04.2017..
 */

public class RvHolder extends RecyclerView.ViewHolder {

    ImageView image;

    public RvHolder(View itemView) {
        super(itemView);

        image = (ImageView) itemView.findViewById(R.id.iv_page_chapter_activity_custom_row);
    }
}
