package com.example.daca.skymanga.chapterlist.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.daca.skymanga.R;



/**
 * Created by daca-pc on 23.04.2017..
 */

public class RvHolder extends RecyclerView.ViewHolder {

    TextView title;
    TextView chapterNumber;
    ImageView chapterImage;

    public RvHolder(View itemView) {
        super(itemView);

        title = (TextView) itemView.findViewById(R.id.txt_title_chapters_list_custom_row);
        chapterNumber = (TextView) itemView.findViewById(R.id.txt_number_chapters_list_custom_row);
        chapterImage = (ImageView) itemView.findViewById(R.id.iv_chapter_img_chapters_list_custom_row);
    }
}
