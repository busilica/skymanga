package com.example.daca.skymanga.main.recyclerview;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.example.daca.skymanga.main.Manga;
import com.example.daca.skymanga.R;
import com.example.daca.skymanga.chapterlist.ChaptersListActivity;
import com.example.daca.skymanga.util.DBHelper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by daca-pc on 23.04.2017..
 */

public class RvAdapter extends RecyclerView.Adapter<RvHolder> {

    private ArrayList<Manga> list;
    private Activity activity;
    private LayoutInflater inflater;
    private DBHelper dbHelper;

    public RvAdapter(ArrayList<Manga> list, Activity activity, DBHelper dbHelper) {
        this.list = list;
        this.activity = activity;
        inflater = activity.getLayoutInflater();
        this.dbHelper = dbHelper;
    }

    @Override
    public RvHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.activity_main_custom_row, parent, false);
        return new RvHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RvHolder holder, int position) {
        final Manga manga = list.get(position);
        String title = manga.getTitle();
        String image = manga.getImage();
        String url = "http://cdn.mangaeden.com/mangasimg/98x/" + image;
        Picasso.with(activity).load(url).into(holder.image);
        holder.title.setText(title);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = manga.getId();
                activity.startActivity(new Intent(activity, ChaptersListActivity.class)
                                                  .putExtra("id", id));
            }
        });

        if (!dbHelper.getRow(manga.getId())){
            holder.star.setTag("off");
            holder.star.setImageResource(android.R.drawable.btn_star_big_off);
        }else {
            holder.star.setTag("on");
            holder.star.setImageResource(android.R.drawable.btn_star_big_on);
        }

        holder.star.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.star.getTag().equals("off")){
                    holder.star.setImageResource(android.R.drawable.btn_star_big_on);
                    holder.star.setTag("on");
                    dbHelper.insertRow(manga);
                }else if (holder.star.getTag().equals("on")){
                    holder.star.setImageResource(android.R.drawable.btn_star_big_off);
                    holder.star.setTag("off");
                    dbHelper.deleteRow(manga.getId());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
