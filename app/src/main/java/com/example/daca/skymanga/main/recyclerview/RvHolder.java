package com.example.daca.skymanga.main.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.daca.skymanga.R;



/**
 * Created by daca-pc on 23.04.2017..
 */

public class RvHolder extends RecyclerView.ViewHolder {

    TextView title;
    ImageView image;
    ImageButton star;

    public RvHolder(View itemView) {
        super(itemView);

        title = (TextView) itemView.findViewById(R.id.txt_title_custom_row);
        image = (ImageView) itemView.findViewById(R.id.iv_custom_row_main);
        star = (ImageButton) itemView.findViewById(R.id.star_custom_row_main);
    }
}
