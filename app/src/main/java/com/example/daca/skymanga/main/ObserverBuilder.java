package com.example.daca.skymanga.main;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by Daca on 14.04.2017..
 */

public class ObserverBuilder {

    private MainActivity activity;

    public ObserverBuilder(MainActivity activity) {
        this.activity = activity;
    }

    public Observer<ArrayList<Manga>> getMangaObserver(){
        Observer<ArrayList<Manga>> observer = new Observer<ArrayList<Manga>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(ArrayList<Manga> value) {
                Collections.sort(value, new Comparator<Manga>() {
                    @Override
                    public int compare(Manga o1, Manga o2) {
                        return Integer.valueOf(o2.getHits()).compareTo(Integer.valueOf(o1.getHits()));
                    }
                });
                activity.setupRvAdapter(value);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
        return observer;
    }
}
