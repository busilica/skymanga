package com.example.daca.skymanga.main;

import org.json.JSONArray;


/**
 * Created by daca-pc on 23.04.2017..
 */

public class Manga {

    private String title;
    private String image;
    private String id;
    private String hits;
    private JSONArray category;

    public Manga(){

    }

    public Manga(String title, String image, String id, String hits, JSONArray category) {
        this.title = title;
        this.image = image;
        this.id    = id;
        this.hits  = hits;
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHits() {
        return hits;
    }

    public void setHits(String hits) {
        this.hits = hits;
    }

    public JSONArray getCategory() {
        return category;
    }

    public void setCategory(JSONArray category) {
        this.category = category;
    }
}
