package com.example.daca.skymanga.chapterlist;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.daca.skymanga.R;
import com.example.daca.skymanga.chapterlist.recyclerview.RvAdapter;

import com.example.daca.skymanga.util.VolleyLoader;
import java.util.ArrayList;

public class ChaptersListActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RvAdapter adapter;
    private VolleyLoader volleyLoader;
    private ObserverBuilder observerBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //this code must be before onCreate
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        int theme = preferences.getInt("THEME", 0);
        switch (theme){
            case 0:
                setTheme(R.style.ActivityThemeLight);
                break;
            case 1:
                setTheme(R.style.ActivityThemeDark);
                break;
            case 2:
                setTheme(R.style.ActivityThemeSolarizedLight);
                break;
            case 3:
                setTheme(R.style.ActivityThemeNightBlue);
                break;
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chapters_list);

        recyclerView = (RecyclerView) findViewById(R.id.rv_activity_chapters_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        String id  = getIntent().getStringExtra("id");
        String url = "http://www.mangaeden.com/api/manga/" + id;

        volleyLoader    = new VolleyLoader(this);
        observerBuilder = new ObserverBuilder(this);
        volleyLoader.load(observerBuilder.getChaptersListObserver(), url, "loadChaptersList");
    }

    @Override
    protected void onDestroy() {
        adapter         = null;
        volleyLoader    = null;
        observerBuilder = null;
        super.onDestroy();
    }

    public void setupRvAdapter(ArrayList<Chapter> value) {
        adapter = new RvAdapter(value, this);
        recyclerView.setAdapter(adapter);
    }
}
