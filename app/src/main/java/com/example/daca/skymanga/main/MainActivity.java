package com.example.daca.skymanga.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.widget.Spinner;

import com.example.daca.skymanga.R;
import com.example.daca.skymanga.main.recyclerview.*;
import com.example.daca.skymanga.util.DBHelper;
import java.util.ArrayList;


public class MainActivity extends OptionsActivity {
    private RecyclerView recyclerView;
    private RvAdapter adapter;
    private DBHelper dbHelper;
    private ArrayList<Manga> list;
    private ProgressDialog progressDialog;;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //this code must be before onCreate
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        int theme = preferences.getInt("THEME", 0);
        switch (theme){
            case 0:
                setTheme(R.style.ActivityThemeLight);
                break;
            case 1:
                setTheme(R.style.ActivityThemeDark);
                break;
            case 2:
                setTheme(R.style.ActivityThemeSolarizedLight);
                break;
            case 3:
                setTheme(R.style.ActivityThemeNightBlue);
                break;
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper = new DBHelper(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        setupProgressDialog();

        Spinner spinner = (Spinner) findViewById(R.id.toolbar_spinner);
        new SpinnerHelper(this, dbHelper).setupSpinner(spinner);

        recyclerView = (RecyclerView) findViewById(R.id.rv_main);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //String url = "http://www.mangaeden.com/api/list/0/?p=1";
    }

    @Override
    protected void onDestroy() {
        adapter         = null;
        super.onDestroy();
    }

    public void setupRvAdapter(ArrayList<Manga> value) {
        if (adapter != null){
            adapter = null;
        }else{
            list = value;
        }
        adapter = new RvAdapter(value, this, dbHelper);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        int layout = preferences.getInt("LAYOUT", 0);
        switch (layout){
            case 0:
                LinearLayoutManager lManager = new LinearLayoutManager(this);
                recyclerView.setLayoutManager(lManager);
                break;
            case 1:
                StaggeredGridLayoutManager s1Manager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
                recyclerView.setLayoutManager(s1Manager);
                break;
            case 2:
                StaggeredGridLayoutManager s2Manager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
                recyclerView.setLayoutManager(s2Manager);
                break;
        }
        recyclerView.setAdapter(adapter);
        dismissProgressDialog();
    }

    public void setSavedManga(ArrayList<Manga> list){
        RvAdapter adapter = new RvAdapter(list, this, dbHelper);
        recyclerView.setAdapter(adapter);
        dismissProgressDialog();
    }

    public void setupProgressDialog(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading........");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
    }

    public void showProgressDialog(){
        progressDialog.show();
    }

    public void dismissProgressDialog(){
        progressDialog.dismiss();
    }

    public ArrayList<Manga> getList(){
        return list;
    }

    @Override
    public void changeLayout(int layout) {
        switch (layout){
            case 0:
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putInt("LAYOUT", 0);
                editor.commit();
                LinearLayoutManager lManager = new LinearLayoutManager(this);
                recyclerView.setLayoutManager(lManager);
                break;
            case 1:
                SharedPreferences preferences2 = PreferenceManager.getDefaultSharedPreferences(this);
                SharedPreferences.Editor editor2 = preferences2.edit();
                editor2.putInt("LAYOUT", 1);
                editor2.commit();
                StaggeredGridLayoutManager s1Manager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
                recyclerView.setLayoutManager(s1Manager);
                break;
            case 2:
                SharedPreferences preferences3 = PreferenceManager.getDefaultSharedPreferences(this);
                SharedPreferences.Editor editor3 = preferences3.edit();
                editor3.putInt("LAYOUT", 2);
                editor3.commit();
                StaggeredGridLayoutManager s2Manager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
                recyclerView.setLayoutManager(s2Manager);
                break;
        }
    }

    @Override
    public void changeTheme(int theme) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt("THEME", theme);
        editor.commit();
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
