package com.example.daca.skymanga.main;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.daca.skymanga.R;
import com.example.daca.skymanga.util.DBHelper;
import com.example.daca.skymanga.util.VolleyLoader;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Daca on 22.04.2017..
 */


/*
 *   Sets up adapter for spinner, arrayList with items,
 *   adapter for items, spinner color scheme
 *   handles onItemSelected for individual items
 */
public class SpinnerHelper implements AdapterView.OnItemSelectedListener {

    private MainActivity activity;
    private DBHelper dbHelper;

    public SpinnerHelper(MainActivity activity, DBHelper dbHelper) {
        this.activity = activity;
        this.dbHelper = dbHelper;
    }

    public void setupSpinner(Spinner spinner){
        ArrayList<String> spinnerArray = new ArrayList<>();
        spinnerArray.add("Popular");
        spinnerArray.add("Saved");
        spinnerArray.add("Action");
        spinnerArray.add("Adventure");
        spinnerArray.add("Drama");
        spinnerArray.add("Sci-fi");
        spinnerArray.add("Supernatural");
        spinnerArray.add("Psychological");
        spinnerArray.add("Slice of Life");
        spinnerArray.add("Romance");
        spinnerArray.add("School Life");
        spinnerArray.add("Martial Arts");
        spinnerArray.add("Horror");
        spinnerArray.add("Comedy");
        spinnerArray.add("Shounen");
        spinnerArray.add("Sports");
        spinnerArray.add("Tragedy");

        ArrayAdapter<String> adapter = new ArrayAdapter<>(activity, R.layout.spinner_item, spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        Drawable spinnerDrawable = spinner.getBackground().getConstantState().newDrawable();
        spinnerDrawable.setColorFilter(activity.getResources().getColor(R.color.colorWhite),
                PorterDuff.Mode.SRC_ATOP);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            spinner.setBackground(spinnerDrawable);
        }else{
            spinner.setBackgroundDrawable(spinnerDrawable);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        ArrayList<Manga> list1 = activity.getList();
        String selectedItem = parent.getItemAtPosition(position).toString();
        if (selectedItem.contains("Popular")){
            activity.showProgressDialog();
            if (list1 == null){
                VolleyLoader volleyLoader    = new VolleyLoader(activity);
                ObserverBuilder observerBuilder = new ObserverBuilder(activity);
                String url = "http://www.mangaeden.com/api/list/0/";
                volleyLoader.load(observerBuilder.getMangaObserver(), url, "loadManga");
            }else {
                activity.setupRvAdapter(list1);
            }

        }else if (selectedItem.contains("Saved")){
            activity.showProgressDialog();
            ArrayList<Manga> list = new ArrayList<>();
            list = dbHelper.getSavedManga();
            activity.setupRvAdapter(list);
        }else {
            activity.showProgressDialog();
            ArrayList<Manga> savedList = new ArrayList<>();
            for (int i = 0; i < list1.size(); i++) {
                JSONArray array = list1.get(i).getCategory();
                for (int j = 0; j < array.length(); j++) {
                    try {
                        if (array.getString(j).contains(selectedItem)){
                            savedList.add(list1.get(i));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            activity.setupRvAdapter(savedList);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
