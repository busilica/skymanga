package com.example.daca.skymanga.chapter.recyclerview;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.example.daca.skymanga.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by daca-pc on 24.04.2017..
 */

public class RvAdapter extends RecyclerView.Adapter<RvHolder> {

    private ArrayList<String> list;
    private Activity activity;
    private LayoutInflater inflater;

    public RvAdapter(ArrayList<String> list, Activity activity) {
        this.list = list;
        this.activity = activity;
        inflater = activity.getLayoutInflater();
    }

    @Override
    public RvHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.activity_chapter_custom_row, parent, false);
        return new RvHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RvHolder holder, int position) {
        String image = list.get(position);
        String url = "http://cdn.mangaeden.com/mangasimg/98x/" + image;
        Picasso.with(activity).load(url).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
